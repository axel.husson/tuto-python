import pathlib

chemin = pathlib.Path(__file__).parent
 
d = {"Films": ["Le seigneur des anneaux",
               "Harry Potter",
               "Moon",
               "Forrest Gump"],
     "Employes": ["Paul",
                  "Pierre",
                  "Marie"],
     "Exercices": ["les_variables",
                   "les_fichiers",
                   "les_boucles"]}

print(chemin)
for key in d:
    print(d[key])
    for i in d[key]:
        dir = pathlib.Path(chemin / key / i)
        print(dir)
        dir.mkdir(parents=True, exist_ok=True)