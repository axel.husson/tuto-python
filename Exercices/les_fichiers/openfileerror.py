import pathlib
file = pathlib.Path(__file__).parent / "prenom2.txt"

try:
    with open(file, "r") as f:
        lines = f.read().splitlines()
except FileNotFoundError:
    print("Le fichier n'existe pas")
except UnicodeDecodeError:
    print("Impossible d'ouvrir le fichier")
else:
    f.close()