import pathlib
file = pathlib.Path(__file__).parent / "prenom.txt"
output = pathlib.Path(__file__).parent / "prenoms_final.txt"
print(file)
with open(file, "r") as f:
    lines = f.read().splitlines()
    #print(lines)

prenoms = []
for line in lines:
    prenoms.extend(line.split())

prenoms_final = [prenom.strip(",. ") for prenom in prenoms]
with open(output, "w") as f:
    f.write("\n".join(sorted(prenoms_final)))