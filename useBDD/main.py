import sqlite3

# Connexion à la base de données
conn = sqlite3.connect('users.db')

# Création de la table si elle n'existe pas
conn.execute('''CREATE TABLE IF NOT EXISTS users
             (id INTEGER PRIMARY KEY AUTOINCREMENT,
              username TEXT NOT NULL,
              password TEXT NOT NULL);''')

# Invitation à saisir des données pour la table
print("Veuillez saisir les informations pour un nouvel utilisateur :")
username = input("Nom d'utilisateur : ")
password = input("Mot de passe : ")

# Insertion des données dans la table
conn.execute("INSERT INTO users (username, password) VALUES (?, ?)", (username, password))
conn.commit()

# Affichage des données de la table
cursor = conn.execute("SELECT * FROM users")
for row in cursor:
    print("ID = ", row[0])
    print("Nom d'utilisateur = ", row[1])
    print("Mot de passe = ", row[2], "\n")

# Fermeture de la connexion à la base de données
conn.close()

print("Les données ont été ajoutées à la table 'users' et affichées ci-dessus.")
