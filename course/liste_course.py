import os
import sys
import json

CUR_DIR = os.path.dirname(__file__)
LISTE_PATH = os.path.join(CUR_DIR, "liste.json")

if os.path.exists(LISTE_PATH):
    with open(LISTE_PATH, "r") as f:
        liste_course = json.load(f)
else:
    liste_course = []

while True:
    print("")
    print("----------------------------")
    print("1 - Ajout sur la liste")
    print("2 - Suppression sur la liste")
    print("3 - Afficher la liste")
    print("4 - Vider la liste")
    print("q - Quitter")
    action = input("Que faire : ")
    if action == '1':
        print("Ajout sur la liste")
        nouveau_item = input("Que voulez vous ajouter : ")
        liste_nouveau_item = nouveau_item.split()
        for i in range(0,len(liste_nouveau_item)):
            liste_course.append(liste_nouveau_item[i])
    elif action == '2':
        print("Suppression de la liste")
        remove_item = input("Que voulez vous supprimer : ")
        liste_remove_item = remove_item.split()
        for i in range(0, len(liste_remove_item)):
            if liste_remove_item[i] in liste_course:
                liste_course.remove(liste_remove_item[i])
            else:
                print(f"{liste_remove_item[i]} non présent dans la liste")
    elif action == '3':
        print(f"Contenu de la liste : ")
        for i in range(0,len(liste_course)):
            print(f"{i+1} - {liste_course[i]}")
    elif action == '4':
        liste_course.clear()
        print("Liste vide")
    elif action == 'q':
        with open(LISTE_PATH, "w") as f:
            json.dump(liste_course, f, indent=4)
        break
