from random import *

pv_joueur = 50
potion_joueur = 3
potion_prise = False

pv_pnj = 50

print("Un PNJ sauvage apparait!")
while True:
    if not potion_prise:
        print("-" * 25)
        print(f"Vos PV : {pv_joueur}\tPV du PNJ : {pv_pnj}")
        print("1 - Attaquer")
        print("2 - Prendre une potion")
        action = input("Que voulez vous faire? : ")
        action = int(action)
        if action == 1:
            print ("Vous attaquez")
            dammage = randint(0,10)
            if dammage == 0:
                print("Mince vous avez loupé votre attaque")
            else:
                print(f"Vous faites {dammage} de dégat au pnj")
                pv_pnj -= dammage
        elif action == 2:
            if potion_joueur > 0:
                print("Vous prenez une potion")
                restaure_pv = randint(15,50)
                print(f"Vous récupérez {restaure_pv} point de vie")
                pv_joueur += restaure_pv
                if pv_joueur > 50:
                    pv_joueur = 50
                potion_joueur -= 1
                potion_prise = True
            else:
                print("Vous n'avez plus de potion")
                continue
        else:
            print("Action incompatible")
            continue
    else:
        print("Vous passez votre tour...")
        potion_prise = False
    if pv_pnj <= 0:
        print("Vous avez gagné")
        break
    pnj_dammage = randint(0,15)
    if pnj_dammage == 0:
        print("Ouf, le pnj loupé son attaque")
    else:
        print(f"Le pnj vous fait {pnj_dammage} de dégat")
        pv_joueur -= pnj_dammage

    if pv_joueur <= 0:
        print("Vous êtes mort")
        break
