import openai

# Configurez votre clé d'API
openai.api_key = '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'

# Fonction pour générer une réponse de ChatGPT
def generer_reponse(question):
    response = openai.Completion.create(
        engine='text-davinci-003',
        prompt=question,
        max_tokens=100,
        temperature=0.7,
        n=1,
        stop=None,
        timeout=10
    )
    return response.choices[0].text.strip()

# Exemple d'utilisation
question_utilisateur = input("Posez une question : ")
reponse_chatgpt = generer_reponse(question_utilisateur)
print(reponse_chatgpt)