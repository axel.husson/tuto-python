import logging

logging.basicConfig(level=logging.WARNING)

logging.debug("Message de debug")
logging.info("Message d'info")
logging.warning("message de warning")
logging.error("Message d'erreur")
logging.critical("Message critique")